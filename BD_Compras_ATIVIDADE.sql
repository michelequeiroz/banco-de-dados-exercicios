
--1) Selecione todos os campos de todas as tabelas	
SELECT * FROM cliente;
SELECT * FROM vendedor;
SELECT * FROM produto;
SELECT * FROM pedido;
SELECT * FROM item_pedido;

--2) Selecione os campos select descricao_produto, unidade, val_unitario e ordene a exibi��o por descricao_produto
 SELECT descricao_produto, unidade, val_unitario
 FROM produto
 ORDER BY descricao_produto ASC;

--3) Selecione nome_cliente, endereco  da tabela de clientes
SELECT nome_cliente, endereco 
FROM cliente;

--4) Selecione descricao_produto, val_unitario da tabela produto onde os nomes comecem com M
SELECT descricao_produto,  val_unitario 
FROM produto 
WHERE descricao_produto LIKE 'M%';

--5)  Selecione cidade, uf, nome_cliente  da tabela de clientes 
--onde a cidade seja S�o Paulo ou a UF seja igual a Paran� e ordene por nome do cliente.
SELECT cidade, uf, nome_cliente
FROM cliente
WHERE cidade = 'Sao Paulo' OR uf = 'PR'
ORDER BY nome_cliente ASC;

--6) Selecione todas as informa��es de pedido onde o prazo de entrega seja diferente de 15 e ordene por n�mero de pedido em ordem descendente
SELECT * FROM pedido 
WHERE prazo_entrega <> 15
ORDER BY num_pedido DESC;

--7) Selecione codigo_produto, descricao_produto da tabela produtos onde o valor unit�rio esteja entre 0.32 e 2
SELECT codigo_produto, descricao_produto
FROM produto
WHERE val_unitario BETWEEN 0.32 AND 2;

--8) Selecione e exiba todas as informa��es da tabela de produtos onde a unidade comece com K
SELECT * FROM produto
WHERE unidade LIKE 'K%';

--9) Mostre todos os dados dos vendedores cujo nome n�o comece com as letras "JO"
SELECT * FROM vendedor
WHERE nome_vendedor NOT LIKE 'JO%';

--10) Mostre todos os dados dos vendedores que a faixa de comiss�o seja A ou B
SELECT * FROM vendedor
WHERE faixa_comissao IN ('A','B');

--11) Mostre todos os dados dos cliente que tem o IE (inscri��o estadual) em branco
SELECT * FROM cliente
WHERE IE IS NULL;

--12) Exiba nome, cidade e UF dos cliente em ordem alfab�tica pela cidade
SELECT cidade, nome_cliente, UF
FROM cliente
ORDER BY cidade ASC;

--13) Selecione a descricao_produto, val_unitario da tabela produtos onde a unidade seja igual a M  e ordene a exibi��o por val_unitario asc
SELECT descricao_produto, val_unitario, unidade
FROM produto
WHERE unidade = 'M'
ORDER BY val_unitario asc;

--14) Selecione o menor e o maior sal�rio fixo para os vendedores

SELECT max(salario_fixo) AS Maior_salario_fixo,
min(salario_fixo) AS Menor_salario_fixo from vendedor;

--15)  Selecione e conte  quantidade e exiba como Quantidade Total 
--e o c�digo de produtos da tabela item do  pedido onde o codigo_produto = '3';

--SELECT COUNT(*) quantidade, codigo_produto
--FROM item_pedido
--WHERE codigo_produto = '3';

--16) Selecione a m�dia dos sal�rios dos vendedores da tabela vendedor
SELECT AVG(salario_fixo ) AS MEDIA_SALARIO
FROM vendedor;

--17) Selecione nome_vendedor, salario_fixo da tabela vendedor onde o 
--sal�rio fixo seja maior ou igual a 2500
SELECT nome_vendedor, salario_fixo
FROM vendedor
WHERE salario_fixo >= 2500;