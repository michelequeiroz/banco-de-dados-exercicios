Create database venda_imobiliaria

CREATE TABLE Proprietario(
CodigoProprietario INTEGER NOT NULL PRIMARY KEY IDENTITY, --Código autoincrementado com chave Primaria
Nome VARCHAR (50) NOT NULL,
CPF VARCHAR(11) NOT NULL,
Endereco VARCHAR (30) NOT NULL,
Cep VARCHAR(8) NOT NULL,
Cidade VARCHAR(30) NOT NULL,
Estado VARCHAR(30) NOT NULL,
Telefone VARCHAR(11) NOT NULL
);


CREATE TABLE Inquilino(
CodigoInquilino INTEGER NOT NULL PRIMARY KEY IDENTITY, --Código autoincrementado com chave Primaria
Nome VARCHAR(50) NOT NULL,
CPF VARCHAR(11) NOT NULL
);


CREATE TABLE Imovel(
CodigoImovel INTEGER NOT NULL PRIMARY KEY IDENTITY, --Código autoincrementado com chave Primaria
Endereco VARCHAR(30) NOT NULL,
Cidade VARCHAR(30) NOT NULL,
Estado VARCHAR(30) NOT NULL,
Cep VARCHAR(8) NOT NULL,
Locado BIT NOT NULL, --Indica se o imóvel está locado (0 = Não, 1 = Sim)
CodigoProprietario INTEGER NOT NULL FOREIGN KEY REFERENCES Proprietario(CodigoProprietario) -- Chave estrangeira para o Proprietario
);


CREATE TABLE locacao(
CodigoContrato INTEGER NOT NULL PRIMARY KEY IDENTITY, --Código autoincrementado com chave Primaria
ValorAluguel MONEY NOT NULL,
TaxaAdministrativa MONEY NOT NULL,
Inicio DATETIME NOT NULL, 
Termino DATETIME NOT NULL, 
CodigoImovel INTEGER NOT NULL FOREIGN KEY REFERENCES Imovel(CodigoImovel), -- Chave estrangeira para o Imovel
CodigoInquilino INTEGER NOT NULL FOREIGN KEY REFERENCES Inquilino(CodigoInquilino) -- Chave estrangeira para o Inquilino
);
