create database compras;
--use compras;
create table cliente (
codigo_cliente integer not null primary key,
nome_cliente varchar (20),
endereco varchar(30),
cidade varchar(15),
cep varchar(8),
UF varchar(2),
CNPJ varchar(20),
IE varchar(20)
);

create table vendedor(
codigo_vendedor integer not null primary key,
nome_vendedor varchar(20),
salario_fixo DECIMAL(10, 2),
faixa_comissao varchar(1)
);

create table produto(
codigo_produto integer not null primary key,
unidade varchar(3),
descricao_produto varchar(30),
val_unitario DECIMAL(10, 2)
);

create table pedido(
num_pedido integer not null primary key,
prazo_entrega integer not null,
codigo_cliente integer not null,
codigo_vendedor integer not null,
foreign key (codigo_cliente) references cliente (codigo_cliente),
foreign key (codigo_vendedor) references vendedor (codigo_vendedor)
);

create table item_pedido(
num_pedido integer not null,
codigo_produto integer not null,
quantidade integer,
foreign key (num_pedido) references pedido (num_pedido),
foreign key (codigo_produto) references produto (codigo_produto)
);

--Inserts - inserir 10 clientes:

insert into cliente values (1,'Ana','Rua 17, 19','Niteroi','24358310','RJ','12113231/0001-34','2134');
insert into cliente values (2,'Flavio','Av. Presidente Vargas, 10','Sao Paulo','11467210','SP','33333333/0001-55','4321');
insert into cliente values (3,'Jorge','Rua Caiapo,13','Curitiba','30335780','PR','12113231/0001-34','2134');
insert into cliente values (4,'Lucia','Rua Itabira, 123 - Loja 9','Belo Horizonte','45675900','MG','44444444/0001-99','7890');
insert into cliente values (5,'Mauricio','Av. Paulista, 1236 - sala 2345','Sao Paulo','11645320','SP','77777777/0001-45','4671');
insert into cliente values (6,'Edmar','Rua da Praia, s/n','Salvador','80670600','BA','22222222/0001-87','6543');
insert into cliente values (7,'Rodolfo','Largo da Lapa, 27 - sobrado','Rio de Janeiro','21346700','RJ','55555555/0001-07','9080');
insert into cliente values (8,'Beth','Av. Climerio, 45','Sao Paulo','11234750','SP','66666666/0001-81','1235');
insert into cliente values (9,'Paulo','Travessa Moraes c/3','Londrina','33619860','PR','11111111/0001-67','6754');
insert into cliente values (10,'Livia','Av. Beira Mar, 1256','Florianopolis','76812120','SC','22113344/0001-89','7789');
insert into cliente values (11,'Susana','Rua Lopes Mendes,11','Niteroi','24360510','RJ','12113231/0001-34','2434');
insert into cliente values (12,'Renato','Rua Meireles,123 -bl.2-sl.345','Sao Paulo','11257890','SP','55446677/0001-21','1287');
insert into cliente values (13,'Sebastiao','Rua da Igreja, 10','Uberaba','45743210','MG','99775533/0001-36','9878');
insert into cliente values (14,'Jose','Quadra 3, bl.03, sala 1003','Brasilia','70468670','DF','22446688/0001-73','6577');


--Inserts - inserir 10 vendedores:
insert into vendedor values(1,'Josue',1800.00,'C');
insert into vendedor values(2,'Carlos',2490.00,'A');
insert into vendedor values(3,'Joao',2780.00,'C');
insert into vendedor values(4,'Antonio',9500.00,'C');
insert into vendedor values(5,'Felipe',4600.00,'A');
insert into vendedor values(6,'Joana',2300.00,'A');
insert into vendedor values(7,'Joao Paulo',2650.00,'C');
insert into vendedor values(8,'Josias',870.00,'B');
insert into vendedor values(9,'Marcelo',2930.00,'B');
insert into vendedor values(10,'Marcela',2830.00,'B');

--Inserts - inserir 10 Produtos:
Insert into produto values (1,'KG','Queijo',0.97);
Insert into produto values (2,'BAR','Chocolate',0.87);
Insert into produto values (3,'L','Vinho',2.00);
Insert into produto values (4,'M','Linho',0.11);
Insert into produto values (5,'SAC','Acucar',0.30);
Insert into produto values (6,'M','Linha',1.80);
Insert into produto values (7,'G','Ouro',6.18);
Insert into produto values (8,'M','Madeira',0.25);
Insert into produto values (9,'M','Cano',1.97);
Insert into produto values (10,'M','Papel',1.05);

--Inserts - inserir Pedidos:
Insert into pedido values (1,20,7,1);
Insert into pedido values (2,20,1,7);
Insert into pedido values (3,15,1,7);
Insert into pedido values (4,20,1,5);
Insert into pedido values (5,20,1,7);
Insert into pedido values (6,15,2,6);
Insert into pedido values (7,30,3,7);
Insert into pedido values (8,30,5,9);
Insert into pedido values (9,20,7,1);
Insert into pedido values (10,30,8,2);
Insert into pedido values (11,15,10,4);
Insert into pedido values (12,20,11,4);
Insert into pedido values (13,20,11,3);
Insert into pedido values (14,20,11,3);
Insert into pedido values (15,20,11,3);
Insert into pedido values (16,15,12,8);
Insert into pedido values (17,30,13,9);
Insert into pedido values (18,10,7,3);

--Inserts - inserir Item_Pedidos:
Insert into item_pedido values
(1,1,10),
(1,2,35),
(2,10,20),
(3,2,9),
(3,3,18),
(3,7,5),
(9,10,5),
(5,8,8),
(5,2,7),
(5,10,3),
(5,1,10),
(5,3,30),
(7,6,32),
(8,2,6),
(6,3,45),
(10,2,20),
(10,3,10),
(11,3,10),
(12,1,10),
(12,3,70),
(13,6,37),
(14,10,40),
(15,4,10),
(15,10,35),
(15,6,18),
(16,7,17),
(17,10,40),
(17,7,6),
(17,4,10),
(17,6,43),
(4,7,8);

